package br.com.caelum.livraria.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped //javax.enterprise.context.SessionScoped
public class TemaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String tema = "pepper-grinder";

	public String getTema() {
		return tema;
	}

	public void setTema(String tema) {
		this.tema = tema;
	}
	// OS MELHORES
	// blitzer cupertino dark-hive hot-sneaks humanity pepper-grinder redmond rocket start sunny vader
	public List<String> getTemas() {
        return Arrays.asList("aristo", "black-tie", "blitzer", "bluesky", "casablanca", "cupertino", "dark-hive",
                "dot-luv", "eggplant", "excite-bike", "flick", "glass-x", "hot-sneaks", "humanity", "le-frog",
                "midnight", "mint-choc", "overcast", "pepper-grinder", "redmond", "rocket", "sam", "smoothness",
                "south-street", "start", "sunny", "swanky-purse", "trontastic", "ui-darkness", "ui-lightness", "vader");

	}
	
}
